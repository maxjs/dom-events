module.exports = {
  collectCoverageFrom: [
    'src/**/*.{js|ts,jsx|tsx}',
    '!src/**/*.test.{js|ts,jsx|tsx}',
    '!src/*/RbGenerated*/*.{js|ts,jsx|tsx}',
    '!src/app.{js|ts}',
    '!src/global-styles.{js|ts}',
    '!src/*/*/Loadable.{js|ts,jsx|tsx}',
  ],
  globals: {
    'ts-jest': {
      babelConfig: true,
    },
  },
  preset: 'ts-jest/presets/js-with-babel',
  transform: {
    "^.+\\.[t|j]sx?$": "babel-jest"
  },
  setupFiles: [],
  testRegex: '_?_?tests_?_?/.*\\.test\\.(js|tsx|ts?)$',
  snapshotSerializers: [],
};
