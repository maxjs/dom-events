import {registerPrototypes} from "./register-prototypes";

export * from "./dom-observer";
export * from "./register-prototypes";

registerPrototypes();
