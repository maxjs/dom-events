import { DOM_MUTATION } from './register-prototypes';

export const mutationObserverConfiguration = {
  attributes: true,
  childList: true,
  characterData: true,
  subtree: true,
  attributeOldValue: true,
  characterDataOldValue: true,
};

export const createMutationObserver = (observationTarget = document.body) =>
  new MutationObserver(function (mutations) {
    observationTarget.dispatchEvent(
      new CustomEvent(DOM_MUTATION, { detail: mutations }),
    );
  });
