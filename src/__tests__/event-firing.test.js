import {beforeAll, describe, it} from "@jest/globals";
import {DOM_MUTATION, registerPrototypes, stopAllObservers} from "../register-prototypes";
import {createMutationObserver} from "../dom-observer";

describe('Testing if mutations are fired', () => {
  const myDivTag = document.createElement('div');

  beforeAll(() => {
    stopAllObservers();
    document.body.append(myDivTag);
    registerPrototypes();
  });

  it('listener is firing when element is added', (done) => {
    const newNode = document.createElement('div');

    document.body.addEventListener(DOM_MUTATION, (event) => {
      expect(event.detail[0].addedNodes[0]).toBe(newNode);
      done();
    }, true);

    myDivTag.append(newNode);
  });
});