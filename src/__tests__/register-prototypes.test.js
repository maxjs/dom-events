import {
  DOM_MUTATION,
  registerPrototypes,
  stopAllObservers,
} from '../register-prototypes';

import { afterEach, beforeAll, describe, it, jest } from '@jest/globals';
import {
  createMutationObserver,
  mutationObserverConfiguration,
} from '../dom-observer';

jest.mock('../dom-observer');

describe('register-prototypes', () => {
  const myDivTag = document.createElement('div');
  beforeAll(() => {
    document.body.append(myDivTag);
    registerPrototypes();
  });

  it('test if observation is started when adding listener and original addEventListener method is called', () => {
    const observe = jest.fn();
    const disconnect = jest.fn();

    const handler = jest.fn();
    createMutationObserver.mockImplementation(() => ({
      observe,
      disconnect,
    }));
    const divHandler = jest.fn();
    //adding 3 times same handler
    document.body.addEventListener(DOM_MUTATION, handler);
    document.body.addEventListener(DOM_MUTATION, handler);
    document.body.addEventListener(DOM_MUTATION, handler);

    //removing 3 times same handler
    document.body.removeEventListener(DOM_MUTATION, handler);
    document.body.removeEventListener(DOM_MUTATION, handler);
    //up to here disconnect shouldn't have been called...
    expect(disconnect).not.toHaveBeenCalled();
    document.body.removeEventListener(DOM_MUTATION, handler);
    expect(disconnect).toHaveBeenCalledTimes(1);

    expect(createMutationObserver).toHaveBeenCalledTimes(1);
    expect(createMutationObserver).toHaveBeenCalledWith(document.body);
    expect(observe).toHaveBeenCalledWith(
      document.body,
      mutationObserverConfiguration,
    );
    expect(observe).toHaveBeenCalledTimes(1);

    //This should call observe again as there is one addEventListner on document.body again...
    document.body.addEventListener(DOM_MUTATION, handler);
    expect(observe).toHaveBeenCalledTimes(2);
    expect(observe).toHaveBeenCalledWith(
      document.body,
      mutationObserverConfiguration,
    );

    myDivTag.addEventListener(DOM_MUTATION, divHandler);
    myDivTag.addEventListener(DOM_MUTATION, handler);

    expect(observe).toHaveBeenCalledTimes(3);
    expect(observe).toHaveBeenCalledWith(
      myDivTag,
      mutationObserverConfiguration,
    );
    expect(createMutationObserver).toHaveBeenCalledTimes(2);
    expect(createMutationObserver).toHaveBeenCalledWith(myDivTag);

    myDivTag.removeEventListener(DOM_MUTATION, divHandler);
    myDivTag.removeEventListener(DOM_MUTATION, handler);
  });

  it('observer should not be created again if more handlers are added', () => {
    const handler = jest.fn();
    document.body.addEventListener(DOM_MUTATION, handler);
    expect(createMutationObserver).toHaveBeenCalledTimes(2);
  });
});
