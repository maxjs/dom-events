import {
  createMutationObserver,
  mutationObserverConfiguration,
} from './dom-observer';
import { addToObjectCounter, reset } from './object-counter';

export const DOM_MUTATION = 'domMutation';
const observationTargetToObserverMap = new Map();

let isPrototypesRegistered = false;

const getMutationObserverByObservationTarget = (observationTarget) =>
  observationTargetToObserverMap.get(observationTarget);

const createMutationObserverByObservationTarget = (observationTarget) => {
  const mutationObserver = createMutationObserver(observationTarget);
  observationTargetToObserverMap.set(observationTarget, mutationObserver);

  return mutationObserver;
};

export const startObserving = (observationTarget = document.body) => {
  let mutationObserver = getMutationObserverByObservationTarget(
    observationTarget,
  );
  if (!mutationObserver) {
    mutationObserver = createMutationObserverByObservationTarget(
      observationTarget,
    );
  }
  mutationObserver.observe(observationTarget, mutationObserverConfiguration);
};

export const stopObserving = (observationTarget = document.body) => {
  const mutationObserver = getMutationObserverByObservationTarget(
    observationTarget,
  );
  if (mutationObserver) {
    mutationObserver.disconnect();
  }
};

export const registerAddEventListenerPrototype = () => {
  const addEventListener = EventTarget.prototype.addEventListener;
  EventTarget.prototype.addEventListener = function () {
    if (arguments[0] === DOM_MUTATION) {
      const handler = arguments[1];
      const handlerCount = addToObjectCounter(handler, 1, this);
      if (handlerCount === 1) {
        const observationTargetCounter = addToObjectCounter(this, 1);
        if (observationTargetCounter === 1) {
          startObserving(this);
        }
      }
    }
    return addEventListener(arguments[0], arguments[1], arguments[2]);
  };
};

export const registerRemoveEventListenerPrototype = () => {
  const removeEventListener = EventTarget.prototype.removeEventListener;
  EventTarget.prototype.removeEventListener = function () {
    if (arguments[0] === DOM_MUTATION) {
      const handler = arguments[1];
      const handlerCount = addToObjectCounter(handler, -1, this);
      if (handlerCount === 0) {
        const observationTargetCounter = addToObjectCounter(this, -1);
        if (observationTargetCounter === 0) {
          stopObserving(this);
        }
      }
    }
    return removeEventListener(arguments[0], arguments[1], arguments[2]);
  };
};

export const registerPrototypes = () => {
  if (!isPrototypesRegistered) {
    registerAddEventListenerPrototype();
    registerRemoveEventListenerPrototype();
    isPrototypesRegistered = true;
  }
};

export const stopAllObservers = () => {
  observationTargetToObserverMap.forEach((mutationObserver) => {
    mutationObserver.disconnect()
  });
  reset();
};
