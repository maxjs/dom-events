const defaultContext = {};
const contextMapMap = new Map();

const getContextMap = (context = defaultContext) => {
  let contextMap = contextMapMap.get(context);
  if (!contextMap) {
    contextMap = new Map();
    contextMapMap.set(context, contextMap);
  }
  return contextMap;
};

export const getObjectCounter = (objectToCount, context = defaultContext) =>
  getContextMap(context).get(objectToCount) ?? 0;

export const addToObjectCounter = (
  objectToCount,
  summand = 0,
  context = defaultContext,
) => {
  let newCounter = getObjectCounter(objectToCount, context) + summand;
  getContextMap(context).set(objectToCount, newCounter);
  return newCounter;
};

export const reset = () => contextMapMap.clear();
