const path = require('path');

module.exports = {
  parser: '@typescript-eslint/parser',
  extends: [
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ['react-hooks'],
  env: {
    jest: true,
  },
  rules: {
    'prettier/prettier': 'error',
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    '@typescript-eslint/no-empty-interface': 'off',
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: './internals/webpack/build/webpack.production.js',
      },
      'import/resolver': {
        node: true,
        typescript: {},
        'eslint-import-resolver-lerna': {
          packages: path.resolve(__dirname, 'packages'),
        },
      },
    },
  },
};
